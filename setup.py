try:
	from setuptools import setup
except ImportError:
	from distutils.core import setup
	
def readme():
    with open('README.rst') as f:
        return f.read()
	
setup(name='lidar',
      version='0.0.1',
      description='intersection_sight_distance',
      long_description=readme(),
      classifiers=[],
      keywords='',
      url='',
      author='J. C. Koch',
      author_email='jckoch@ualberta.ca',
      license='GPL',
      packages=[''],
      install_requires=[
              'laspy',
              'numpy',
      ],
      test_suite='nose.collector',
      tests_require=['nose','nose-cover3'],
      entry_points={},
      include_package_data=True,
      zip_safe=False)
