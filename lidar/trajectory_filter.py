def trajectory_filter(idx):
    filtered_x = scaled_x[idx]
    filtered_y = scaled_y[idx]
    filtered_z = scaled_z[idx]
    filtered_t = gps_time[idx]
    data = numpy.stack((filtered_x,filtered_y,filtered_z,filtered_t))
    data = data.transpose()
    return(data)  
