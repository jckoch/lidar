def scale_x_dim(las_file):
    x_dim = las_file.X
    scale = las_file.header.scale[0]
    offset = las_file.header.offset[0]
    return(x_dim*scale + offset)

def scale_y_dim(las_file):
    y_dim = las_file.Y
    scale = las_file.header.scale[1]
    offset = las_file.header.offset[1]
    return(y_dim*scale + offset)

def scale_z_dim(las_file):
    z_dim = las_file.Z
    scale = las_file.header.scale[2]
    offset = las_file.header.offset[2]
    return(z_dim*scale + offset)
