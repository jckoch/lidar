import laspy
import numpy
import scale_coord
import trajectory_filter

if __name__ == '__main__':
    # Opening a las file
    infile = laspy.file.File("01410E_C1R1_32000_34511.las", mode='r')
    # point format
    pointformat = infile.point_format
    for spec in infile.point_format:
        print(spec.name)
    # extract scan_angle_rank, pnt_src_id, gps_time
    angle = infile.scan_angle_rank
    scanner = infile.pt_src_id
    gps_time = infile.gps_time
    # filter data for angle == 0 and scanner == 1
    idx = numpy.where((angle == 0) & (scanner == 1))[0]
    # scaled x,y,z data
    scaled_x = scale_x_dim(infile)
    scaled_y = scale_y_dim(infile)
    scaled_z = scale_z_dim(infile)
    # Create numpy 2d array of x,y,z,time rows
    data = trajectory_filter(idx)
    shape = data.shape
    # Sort data by gps_time
    data[data[:,3].argsort()]
